# DPU PoC Kit Roles

Each folder in this directory provides configuration and files to preform DPU or x86 configuration and administration tasks.

Each directory is designed to be a reusable component and does not have any outside dependencies.

- 'create_sf_pair' - create Scalable Functions on the DPU
- 'create_vfs' - Component to create Virtual Functions on the DPU
- 'deploy_doca_ar_container' - Deploys the Application Recognition container from NGC
- 'deploy_doca_devel_container' - Deploys the DOCA development container from NGC
- 'deploy_doca_ips_container' - Deploys the IPS container from NGC
- 'deploy_doca_telemetry_container' - Deploys the DOCA Telemetry container from NGC
- 'deploy_doca_url_filter_container' - Deploys the URL Filter container from NGC
- 'deprecated' - a folder containing deprecated playbooks and components
- `dhcp_server` - A basic `isc-dhcp` setup for lab environments.
- `embedded_mode` - Enables DPU "Embedded" mode through the use of [MST](https://docs.mellanox.com/display/MFTv4160/Mellanox+Software+Tools+%28mst%29+Service)
- 'grafana-monitoring' - Monitor the DPU via Grafana Cloud
- `install_bfb` - Downloads and installs the BlueField BFB image over the RSHIM interface.
- `install_dpu_doca` - Installs the DOCA components on a BlueField-2 DPU.
- 'install_dpu_dpdk' - Setup "hugepages" and verifies testpmd
- 'install_ngc_cli' - Installs the NGC CLI onto the DPU
- 'install_server_dnsmasq' - Playbook to NAT the DPU's egress traffic from the rshim interface
- `install_server_doca` - Installs the DOCA components on an Ubuntu 20.04 x86 host.
- `install_utility_software` - Installs packages and configurations to improve the user experience.
- 'ktls' - KTLS offload proof of concept
- `manage_bf2_fw` - Checks and updates DPU Firmware to the latest version.
- 'ngc_containerd_setup' - Component to setup and configure Containerd and Docker on the DPU
- 'networking' - Configures the IP address of the x86 and the DPU rshim
- `onward` - An internal utility to handle "Yes/No" user input.
- `precheck` - Verifies the x86 is supported by the PoC Kit
- `reboot_os` - Reboots a host and waits for it to be accessible again.
- `remove_ovs` - Removes all configured OVS bridges.
- `reset_dpu_configuration` - Resets BF FW to factory defaults
- `restricted_mode_disable` - Disables DPU restricted mode, returning the DPU to the default `privileged` mode.
- `restricted_mode_enable` - Enables DPU restricted mode.
- `separated_mode` - Configures the DPU in `Separated Mode`.
